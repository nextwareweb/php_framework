<?php defined('loaded') or die();

    /**
     * This is a debug function.
     * @param $obj
     * @param bool $die
     */
    function pre($obj, $die = null) {
        echo "<pre>";
        var_dump($obj);
        echo "</pre>";
        if ($die) die();
    }

    /**
     * Link to main web object.
     * @return mixed.
     */
    function &core() {
        $coreName = APP_NAME;
        global $$coreName;
        return $$coreName;
    }

    /**
     * Create a variable length and alphanumeric token.
     * @param int $length
     * @param string $chars
     * @return string
     */
    function token($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890') {
        $string = '';
        for ($i = 1; $i <= $length; $i++) $string .= $chars[rand(0, (strlen($chars) - 1))];
        return $string;
    }