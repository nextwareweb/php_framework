<?php defined('loaded') or die();

    function __autoload($file) {
        $file = str_replace('_', '.', $file);
        $segments = explode('.', $file);

        /* core includes */
        if (count($segments) == 2) {
            switch($segments[count($segments)-1]) {
                case 'class':
                    require(APP_CLASS_PATH.$file.'.php');
                    break;
                case 'component':
                    require(APP_COMPONENT_PATH.$file.'.php');
                    break;
            }
        }
    }