<?php

    /**
     * Core Defines
     */
    define('loaded', true);

    define('APP_VERSION', 1.01);
    define('APP_NAME', 'core');

    define('ROOT_PATH', dirname(dirname(__FILE__)).'/');
    define('APP_PATH', dirname(__FILE__).'/');
    define('APP_CONFIG_PATH', APP_PATH.'config/');
    define('APP_CLASS_PATH', APP_PATH.'class/');
    define('APP_COMPONENT_PATH', APP_PATH.'component/');
    define('APP_LOGS_PATH', APP_PATH.'logs/');
    define('MODULES_PATH', ROOT_PATH.'modules/'.$workspace.'/');
    define('VENDORS_PATH', ROOT_PATH.'vendors/');