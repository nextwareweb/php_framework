<?php defined('loaded') or die();

    class config_class extends class_component {
        public $items = array();

        public function init() {
            /* Parse Workspace Config file */
            $configFile = APP_CONFIG_PATH.core()->workspace.'.yml';
            $this->parseConfigFile($configFile);
        }

        private function parseConfigFile($file) {
            $data = file_get_contents($file);
            $this->items = core()->parser->yml->parse($data);
        }
    }