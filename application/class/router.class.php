<?php defined('loaded') or die();

    class router_class extends class_component {
        public $module = null;
        public $controller = null;
        public $action = null;
        public $params = null;

        /**
         * Initialize this object.
         */
        public function init() {
            $route = $this->get_current();

            $this->module = (isset($route['module'])) ? $route['module'] : null;
            $this->controller = (isset($route['controller'])) ? $route['controller'] : null;
            $this->action = (isset($route['action'])) ? $route['action'] : null;
            $this->params = (isset($route['params'])) ? $route['params'] : null;
        }

        /**
         * Set the current router
         */
        private function get_current() {
            $route = null;

            /* Get URI */
            $current_script_path = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
            $current_request_uri = str_replace('#'.$current_script_path, '', '#'.$_SERVER['REQUEST_URI']);
            $current_request_uri_segments = explode('?', $current_request_uri, 2);
            $uri = (isset($current_request_uri_segments[0]) ? $current_request_uri_segments[0] : '');

            $route = null;

            if ($uri) {
                /* Parse all modules routes */
                foreach (core()->module->items as $module) {if (count($module->routing)) {
                        foreach ($module->routing as $regex => $values) {
                            preg_match('|^' . $regex . '$|', $uri, $params);

                            if (count($params)) {
                                $route = $values;
                                unset($params[0]);
                                $route['params'] = $params;
                                $route['module'] = $module->name;
                                break;
                            }
                        }
                    }
                }
            } else if (isset(core()->config->items['default_route']) && isset(core()->config->items['default_route']['controller']))  {
                /* Set default controller in workspace config */
                $route = core()->config->items['default_route'];
            }

            if (!$route) {
                /* returns a 404 error */
                core()->error->set(404);
            }

            return $route;
        }

        /**
         * Call the current controller
         */
        public function call_current() {
            $module = core()->module->items[$this->module];

            require($module->path.'controller/'.$this->controller.'.controller.php');
            $class_name = '\\'.str_replace('/', '\\', $this->module).'\controller';
            $controller = new $class_name();

            call_user_func_array(array($controller, $this->action), $this->params);
        }
    }