<?php defined('loaded') or die();

    class core_class extends class_component {

        public $workspace = null;

        /**
         *  Initialize this object.
         */
        public function init($workspace) {
            /* set Workspace */
            $this->workspace = $workspace;

            /* Create Core objects */
            $this->error = new error_class();
            $this->request = new request_class();
            $this->parser = new parser_class();
            $this->config = new config_class();
            $this->module = new module_class();
            $this->router = new router_class();
            $this->view = new view_class();
            $this->renderer = new renderer_class();

            /* Initialize objects */
            $this->error->init();
            $this->request->init();
            $this->config->init();
            $this->module->init();
            $this->renderer->init();
            $this->router->init();

            /* Call to Controller */
            $this->router->call_current();
        }
    }