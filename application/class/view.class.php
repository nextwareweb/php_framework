<?php defined('loaded') or die();

    class view_class extends class_component {

        public $layouts = array();

        public function layout($name) {
            $layout = $this->layouts[$name];

            $module_path = core()->module->items[$layout['module']]->path;
            $layout_path = $module_path.'view/layouts/'.$name.'.tpl';

            if (file_exists($layout_path)) {
                require($layout_path);
            }
        }
    }