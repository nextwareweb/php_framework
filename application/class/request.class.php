<?php defined('loaded') or die();

    class request_class extends class_component {

        private $items = array();

        /**
         *  Initialize this object.
         */
        public function init() {
            /* parse $_REQUEST array */
            if (isset($_REQUEST) && count($_REQUEST)) {
                foreach ($_REQUEST as $key => $value) {
                    $this->set($key, $value);
                }
            }
        }

        /**
         * Set or Delete a request request variable value.
         * @param string $key
         * @param mixed $value
         */
        public function set($key, $value){
            if ($value) {
                $this->items[$key] = $value;
            } elseif (isset($this->items[$key])) {
                unset($this->items[$key]);
            }
        }

        /**
         * Get a request variable value (if exists).
         * @param string $key
         */
        public function get($key) {
            if (isset($this->items[$key])) {
                return $this->items[$key];
            } else {
                return null;
            }
        }
    }