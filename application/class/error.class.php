<?php defined('loaded') or die();

    class error_class extends class_component {

        /**
         * Initialize this object.
         */
        public function init() {
            register_shutdown_function(array($this, 'shutdown_handler'));
            set_error_handler(array($this, 'error_handler'));
            set_exception_handler(array($this, 'exception_handler'));
            ini_set('display_errors', 'off');
            error_reporting(E_ALL);
        }

        /**
         * Sets a user-defined error handler function.
         * @param int $errNo
         * @param string $errStr
         * @param string $errFile
         * @param int $errLine
         * @param array $errContext
         */
        public function error_handler($errNo, $errStr, $errFile, $errLine, $errContext = null) {
            $this->watchdog('ERROR', $errStr);
            $this->exception_handler(new ErrorException($errStr, 0, $errNo, $errFile, $errLine));
        }

        /**
         * Register a function for execution on shutdown.
         */
        public function shutdown_handler() {
            $error = error_get_last();
            if ($error["type"] == E_ERROR)
                $this->error_handler($error["type"], $error["message"], $error["file"], $error["line"]);
        }

        /**
         * Sets a user-defined exception handler function.
         * @param Exception $e
         * @return bool
         */
        public function exception_handler(Exception $e) {
            pre($e);
            return true;
        }

        public function watchdog($type, $message) {
            $line = date('d\/m\/Y H:i:s')."\t".$type."\t".$message."\r\n";
            file_put_contents(APP_LOGS_PATH.'watchdogs.log', $line, FILE_APPEND);
        }

        public function set($err) {
            /* TODO: Hacer un sistema de errores 4xx, 5xx con die */
            pre($err, true);
        }
    }