<?php defined('loaded') or die();

    class module_class extends class_component {

        public $items = array();

        /**
         * Initialize this object.
         */
        public function init() {
            $modules = core()->config->items['modules'];

            foreach ($modules as $module => $enabled) {
                if ($enabled) {
                    /* load module */
                    $this->items[$module] = new module_component($module);
                }
            }

            /* Call Init Hook */
            $this->call_hook('init', $params, function($return){});
        }

        /**
         * Call Modules hooks and return the $params array modified by this hooks.
         * The Callback is called every module hook passing the value returned for the hook in the first argument.
         *
         * @param string $hook
         * @param array $params
         * @param function $callback
         */
        public function call_hook($hook, &$params = array(), $callback = null) {
            if (count($this->items)) {
                foreach ($this->items as $module) {
                    if (method_exists($module->instance, $hook)) {
                        /* TODO: Reeplantearse la forma de llamar los hooks para pasar parametros */
                        $return = $module->instance->$hook($params);

                        if ($callback) {
                            $callback($return);
                        }
                    }
                }
            }
        }
    }