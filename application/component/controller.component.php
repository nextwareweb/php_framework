<?php defined('loaded') or die();

    class controller_component {

        public $layout = 'html5';

        function __construct() {

        }

        public function view($view, $params = array()) {
            core()->view->layout($this->layout);
        }
        
    }