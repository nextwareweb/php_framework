<?php defined('loaded') or die();

    class module_component {

        public $name = '';
        public $path = '';

        public $config = array();
        public $structure = array();
        public $routing = array();
        public $schema = array();

        function __construct($name) {
            $this->name = $name;
            $this->path = MODULES_PATH.$name.'/';

            /* load config */
            $config = array(
                'config' => 'config.yml',
                'structure' => 'structure.yml',
                'routing' => 'routing.yml',
                'schema' => 'schema.yml'
            );

            foreach ($config as $variable => $file) {
                $file_path = $this->path.'config/'.$file;

                /* TODO: Crear dependencias en el config y devolver error si no se cumplen */
                if (file_exists($file_path)) {
                    $data = file_get_contents($file_path);
                    $this->$variable = core()->parser->yml->parse($data);
                }
            }

            /* Parse layouts */
            if (isset($this->structure['layouts']) && is_array($this->structure['layouts']) && count($this->structure['layouts'])) {
                foreach($this->structure['layouts'] as $key => $layout) {
                    $layout['module'] = $this->name;
                    core()->view->layouts[$key] = $layout;
                }

                unset($this->structure['layouts']);
            }

            /* instance module */
            require($this->path.'module.php');
            $class_name = '\\'.str_replace('/', '\\', $this->name).'\module';
            $this->instance = new $class_name();
        }

    }