<?php defined('loaded') or die();

    class yml_component {
        const BLOCK_SCALAR_HEADER_PATTERN = '(?P<separator>\||>)(?P<modifiers>\+|\-|\d+|\+\d+|\-\d+|\d+\+|\d+\-)?(?P<comments> +#.*)?';

        private $offset = 0;
        private $lines = array();
        private $currentLineNb = -1;
        private $currentLine = '';
        private $refs = array();

        /**
         * Constructor.
         *
         * @param int $offset The offset of YAML document (used for line numbers in error messages)
         */
        public function __construct($offset = 0) {
            $this->offset = $offset;
        }

        /**
         * Parses a YAML string to a PHP value.
         *
         * @param string $value                  A YAML string
         * @param bool   $exceptionOnInvalidType true if an exception must be thrown on invalid types (a PHP resource or object), false otherwise
         * @param bool   $objectSupport          true if object support is enabled, false otherwise
         * @param bool   $objectForMap           true if maps should return a stdClass instead of array()
         *
         * @return mixed A PHP value
         *
         * @throws ParseException If the YAML is not valid
         */
        public function parse($value, $exceptionOnInvalidType = false, $objectSupport = false, $objectForMap = false) {
            if (!preg_match('//u', $value)) {
                throw new ParseException('The YAML value does not appear to be valid UTF-8.');
            }

            $this->currentLineNb = -1;
            $this->currentLine = '';
            $value = $this->cleanup($value);
            $this->lines = explode("\n", $value);

            if (2 /* MB_OVERLOAD_STRING */ & (int) ini_get('mbstring.func_overload')) {
                $mbEncoding = mb_internal_encoding();
                mb_internal_encoding('UTF-8');
            }

            $data = array();
            $context = null;
            $allowOverwrite = false;

            while ($this->moveToNextLine()) {
                if ($this->isCurrentLineEmpty()) {
                    continue;
                }

                // tab?
                if ("\t" === $this->currentLine[0]) {
                    throw new ParseException('A YAML file cannot contain tabs as indentation.', $this->getRealCurrentLineNb() + 1, $this->currentLine);
                }

                $isRef = $mergeNode = false;
                if (preg_match('#^\-((?P<leadspaces>\s+)(?P<value>.+?))?\s*$#u', $this->currentLine, $values)) {
                    if ($context && 'mapping' == $context) {
                        throw new ParseException('You cannot define a sequence item when in a mapping');
                    }
                    $context = 'sequence';

                    if (isset($values['value']) && preg_match('#^&(?P<ref>[^ ]+) *(?P<value>.*)#u', $values['value'], $matches)) {
                        $isRef = $matches['ref'];
                        $values['value'] = $matches['value'];
                    }

                    // array
                    if (!isset($values['value']) || '' == trim($values['value'], ' ') || 0 === strpos(ltrim($values['value'], ' '), '#')) {
                        $c = $this->getRealCurrentLineNb() + 1;
                        $parser = new self($c);
                        $parser->refs = &$this->refs;
                        $data[] = $parser->parse($this->getNextEmbedBlock(null, true), $exceptionOnInvalidType, $objectSupport, $objectForMap);
                    } else {
                        if (isset($values['leadspaces'])
                            && preg_match('#^(?P<key>'.Inline::REGEX_QUOTED_STRING.'|[^ \'"\{\[].*?) *\:(\s+(?P<value>.+?))?\s*$#u', $values['value'], $matches)
                        ) {
                            // this is a compact notation element, add to next block and parse
                            $c = $this->getRealCurrentLineNb();
                            $parser = new self($c);
                            $parser->refs = &$this->refs;

                            $block = $values['value'];
                            if ($this->isNextLineIndented()) {
                                $block .= "\n".$this->getNextEmbedBlock($this->getCurrentLineIndentation() + strlen($values['leadspaces']) + 1);
                            }

                            $data[] = $parser->parse($block, $exceptionOnInvalidType, $objectSupport, $objectForMap);
                        } else {
                            $data[] = $this->parseValue($values['value'], $exceptionOnInvalidType, $objectSupport, $objectForMap);
                        }
                    }
                    if ($isRef) {
                        $this->refs[$isRef] = end($data);
                    }
                } elseif (preg_match('#^(?P<key>'.Inline::REGEX_QUOTED_STRING.'|[^ \'"\[\{].*?) *\:(\s+(?P<value>.+?))?\s*$#u', $this->currentLine, $values) && (false === strpos($values['key'], ' #') || in_array($values['key'][0], array('"', "'")))) {
                    if ($context && 'sequence' == $context) {
                        throw new ParseException('You cannot define a mapping item when in a sequence');
                    }
                    $context = 'mapping';

                    // force correct settings
                    Inline::parse(null, $exceptionOnInvalidType, $objectSupport, $objectForMap, $this->refs);
                    try {
                        $key = Inline::parseScalar($values['key']);
                    } catch (ParseException $e) {
                        $e->setParsedLine($this->getRealCurrentLineNb() + 1);
                        $e->setSnippet($this->currentLine);

                        throw $e;
                    }

                    // Convert float keys to strings, to avoid being converted to integers by PHP
                    if (is_float($key)) {
                        $key = (string) $key;
                    }

                    if ('<<' === $key) {
                        $mergeNode = true;
                        $allowOverwrite = true;
                        if (isset($values['value']) && 0 === strpos($values['value'], '*')) {
                            $refName = substr($values['value'], 1);
                            if (!array_key_exists($refName, $this->refs)) {
                                throw new ParseException(sprintf('Reference "%s" does not exist.', $refName), $this->getRealCurrentLineNb() + 1, $this->currentLine);
                            }

                            $refValue = $this->refs[$refName];

                            if (!is_array($refValue)) {
                                throw new ParseException('YAML merge keys used with a scalar value instead of an array.', $this->getRealCurrentLineNb() + 1, $this->currentLine);
                            }

                            foreach ($refValue as $key => $value) {
                                if (!isset($data[$key])) {
                                    $data[$key] = $value;
                                }
                            }
                        } else {
                            if (isset($values['value']) && $values['value'] !== '') {
                                $value = $values['value'];
                            } else {
                                $value = $this->getNextEmbedBlock();
                            }
                            $c = $this->getRealCurrentLineNb() + 1;
                            $parser = new self($c);
                            $parser->refs = &$this->refs;
                            $parsed = $parser->parse($value, $exceptionOnInvalidType, $objectSupport, $objectForMap);

                            if (!is_array($parsed)) {
                                throw new ParseException('YAML merge keys used with a scalar value instead of an array.', $this->getRealCurrentLineNb() + 1, $this->currentLine);
                            }

                            if (isset($parsed[0])) {
                                // If the value associated with the merge key is a sequence, then this sequence is expected to contain mapping nodes
                                // and each of these nodes is merged in turn according to its order in the sequence. Keys in mapping nodes earlier
                                // in the sequence override keys specified in later mapping nodes.
                                foreach ($parsed as $parsedItem) {
                                    if (!is_array($parsedItem)) {
                                        throw new ParseException('Merge items must be arrays.', $this->getRealCurrentLineNb() + 1, $parsedItem);
                                    }

                                    foreach ($parsedItem as $key => $value) {
                                        if (!isset($data[$key])) {
                                            $data[$key] = $value;
                                        }
                                    }
                                }
                            } else {
                                // If the value associated with the key is a single mapping node, each of its key/value pairs is inserted into the
                                // current mapping, unless the key already exists in it.
                                foreach ($parsed as $key => $value) {
                                    if (!isset($data[$key])) {
                                        $data[$key] = $value;
                                    }
                                }
                            }
                        }
                    } elseif (isset($values['value']) && preg_match('#^&(?P<ref>[^ ]+) *(?P<value>.*)#u', $values['value'], $matches)) {
                        $isRef = $matches['ref'];
                        $values['value'] = $matches['value'];
                    }

                    if ($mergeNode) {
                        // Merge keys
                    } elseif (!isset($values['value']) || '' == trim($values['value'], ' ') || 0 === strpos(ltrim($values['value'], ' '), '#')) {
                        // hash
                        // if next line is less indented or equal, then it means that the current value is null
                        if (!$this->isNextLineIndented() && !$this->isNextLineUnIndentedCollection()) {
                            // Spec: Keys MUST be unique; first one wins.
                            // But overwriting is allowed when a merge node is used in current block.
                            if ($allowOverwrite || !isset($data[$key])) {
                                $data[$key] = null;
                            }
                        } else {
                            $c = $this->getRealCurrentLineNb() + 1;
                            $parser = new self($c);
                            $parser->refs = &$this->refs;
                            $value = $parser->parse($this->getNextEmbedBlock(), $exceptionOnInvalidType, $objectSupport, $objectForMap);
                            // Spec: Keys MUST be unique; first one wins.
                            // But overwriting is allowed when a merge node is used in current block.
                            if ($allowOverwrite || !isset($data[$key])) {
                                $data[$key] = $value;
                            }
                        }
                    } else {
                        $value = $this->parseValue($values['value'], $exceptionOnInvalidType, $objectSupport, $objectForMap);
                        // Spec: Keys MUST be unique; first one wins.
                        // But overwriting is allowed when a merge node is used in current block.
                        if ($allowOverwrite || !isset($data[$key])) {
                            $data[$key] = $value;
                        }
                    }
                    if ($isRef) {
                        $this->refs[$isRef] = $data[$key];
                    }
                } else {
                    // multiple documents are not supported
                    if ('---' === $this->currentLine) {
                        throw new ParseException('Multiple documents are not supported.');
                    }

                    // 1-liner optionally followed by newline(s)
                    if (is_string($value) && $this->lines[0] === trim($value)) {
                        try {
                            $value = Inline::parse($this->lines[0], $exceptionOnInvalidType, $objectSupport, $objectForMap, $this->refs);
                        } catch (ParseException $e) {
                            $e->setParsedLine($this->getRealCurrentLineNb() + 1);
                            $e->setSnippet($this->currentLine);

                            throw $e;
                        }

                        if (is_array($value)) {
                            $first = reset($value);
                            if (is_string($first) && 0 === strpos($first, '*')) {
                                $data = array();
                                foreach ($value as $alias) {
                                    $data[] = $this->refs[substr($alias, 1)];
                                }
                                $value = $data;
                            }
                        }

                        if (isset($mbEncoding)) {
                            mb_internal_encoding($mbEncoding);
                        }

                        return $value;
                    }

                    switch (preg_last_error()) {
                        case PREG_INTERNAL_ERROR:
                            $error = 'Internal PCRE error.';
                            break;
                        case PREG_BACKTRACK_LIMIT_ERROR:
                            $error = 'pcre.backtrack_limit reached.';
                            break;
                        case PREG_RECURSION_LIMIT_ERROR:
                            $error = 'pcre.recursion_limit reached.';
                            break;
                        case PREG_BAD_UTF8_ERROR:
                            $error = 'Malformed UTF-8 data.';
                            break;
                        case PREG_BAD_UTF8_OFFSET_ERROR:
                            $error = 'Offset doesn\'t correspond to the begin of a valid UTF-8 code point.';
                            break;
                        default:
                            $error = 'Unable to parse.';
                    }

                    throw new ParseException($error, $this->getRealCurrentLineNb() + 1, $this->currentLine);
                }
            }

            if (isset($mbEncoding)) {
                mb_internal_encoding($mbEncoding);
            }

            return empty($data) ? null : $data;
        }

        /**
         * Returns the current line number (takes the offset into account).
         *
         * @return int The current line number
         */
        private function getRealCurrentLineNb() {
            return $this->currentLineNb + $this->offset;
        }

        /**
         * Returns the current line indentation.
         *
         * @return int The current line indentation
         */
        private function getCurrentLineIndentation() {
            return strlen($this->currentLine) - strlen(ltrim($this->currentLine, ' '));
        }

        /**
         * Returns the next embed block of YAML.
         *
         * @param int  $indentation The indent level at which the block is to be read, or null for default
         * @param bool $inSequence  True if the enclosing data structure is a sequence
         *
         * @return string A YAML string
         *
         * @throws ParseException When indentation problem are detected
         */
        private function getNextEmbedBlock($indentation = null, $inSequence = false) {
            $oldLineIndentation = $this->getCurrentLineIndentation();

            if (!$this->moveToNextLine()) {
                return;
            }

            if (null === $indentation) {
                $newIndent = $this->getCurrentLineIndentation();

                $unindentedEmbedBlock = $this->isStringUnIndentedCollectionItem($this->currentLine);

                if (!$this->isCurrentLineEmpty() && 0 === $newIndent && !$unindentedEmbedBlock) {
                    throw new ParseException('Indentation problem.', $this->getRealCurrentLineNb() + 1, $this->currentLine);
                }
            } else {
                $newIndent = $indentation;
            }

            $data = array();
            if ($this->getCurrentLineIndentation() >= $newIndent) {
                $data[] = substr($this->currentLine, $newIndent);
            } else {
                $this->moveToPreviousLine();

                return;
            }

            if ($inSequence && $oldLineIndentation === $newIndent && isset($data[0][0]) && '-' === $data[0][0]) {
                // the previous line contained a dash but no item content, this line is a sequence item with the same indentation
                // and therefore no nested list or mapping
                $this->moveToPreviousLine();

                return;
            }

            $isItUnindentedCollection = $this->isStringUnIndentedCollectionItem($this->currentLine);

            // Comments must not be removed inside a block scalar
            $removeCommentsPattern = '~'.self::BLOCK_SCALAR_HEADER_PATTERN.'$~';
            $removeComments = !preg_match($removeCommentsPattern, $this->currentLine);

            while ($this->moveToNextLine()) {
                $indent = $this->getCurrentLineIndentation();

                if ($indent === $newIndent) {
                    $removeComments = !preg_match($removeCommentsPattern, $this->currentLine);
                }

                if ($isItUnindentedCollection && !$this->isStringUnIndentedCollectionItem($this->currentLine) && $newIndent === $indent) {
                    $this->moveToPreviousLine();
                    break;
                }

                if ($this->isCurrentLineBlank()) {
                    $data[] = substr($this->currentLine, $newIndent);
                    continue;
                }

                if ($removeComments && $this->isCurrentLineComment()) {
                    continue;
                }

                if ($indent >= $newIndent) {
                    $data[] = substr($this->currentLine, $newIndent);
                } elseif (0 == $indent) {
                    $this->moveToPreviousLine();

                    break;
                } else {
                    throw new ParseException('Indentation problem.', $this->getRealCurrentLineNb() + 1, $this->currentLine);
                }
            }

            return implode("\n", $data);
        }

        /**
         * Moves the parser to the next line.
         *
         * @return bool
         */
        private function moveToNextLine() {
            if ($this->currentLineNb >= count($this->lines) - 1) {
                return false;
            }

            $this->currentLine = $this->lines[++$this->currentLineNb];

            return true;
        }

        /**
         * Moves the parser to the previous line.
         */
        private function moveToPreviousLine() {
            $this->currentLine = $this->lines[--$this->currentLineNb];
        }

        /**
         * Parses a YAML value.
         *
         * @param string $value                  A YAML value
         * @param bool   $exceptionOnInvalidType True if an exception must be thrown on invalid types false otherwise
         * @param bool   $objectSupport          True if object support is enabled, false otherwise
         * @param bool   $objectForMap           true if maps should return a stdClass instead of array()
         *
         * @return mixed A PHP value
         *
         * @throws ParseException When reference does not exist
         */
        private function parseValue($value, $exceptionOnInvalidType, $objectSupport, $objectForMap) {
            if (0 === strpos($value, '*')) {
                if (false !== $pos = strpos($value, '#')) {
                    $value = substr($value, 1, $pos - 2);
                } else {
                    $value = substr($value, 1);
                }

                if (!array_key_exists($value, $this->refs)) {
                    throw new ParseException(sprintf('Reference "%s" does not exist.', $value), $this->currentLine);
                }

                return $this->refs[$value];
            }

            if (preg_match('/^'.self::BLOCK_SCALAR_HEADER_PATTERN.'$/', $value, $matches)) {
                $modifiers = isset($matches['modifiers']) ? $matches['modifiers'] : '';

                return $this->parseBlockScalar($matches['separator'], preg_replace('#\d+#', '', $modifiers), (int) abs($modifiers));
            }

            try {
                return Inline::parse($value, $exceptionOnInvalidType, $objectSupport, $objectForMap, $this->refs);
            } catch (ParseException $e) {
                $e->setParsedLine($this->getRealCurrentLineNb() + 1);
                $e->setSnippet($this->currentLine);

                throw $e;
            }
        }

        /**
         * Parses a block scalar.
         *
         * @param string $style       The style indicator that was used to begin this block scalar (| or >)
         * @param string $chomping    The chomping indicator that was used to begin this block scalar (+ or -)
         * @param int    $indentation The indentation indicator that was used to begin this block scalar
         *
         * @return string The text value
         */
        private function parseBlockScalar($style, $chomping = '', $indentation = 0) {
            $notEOF = $this->moveToNextLine();
            if (!$notEOF) {
                return '';
            }

            $isCurrentLineBlank = $this->isCurrentLineBlank();
            $text = '';

            // leading blank lines are consumed before determining indentation
            while ($notEOF && $isCurrentLineBlank) {
                // newline only if not EOF
                if ($notEOF = $this->moveToNextLine()) {
                    $text .= "\n";
                    $isCurrentLineBlank = $this->isCurrentLineBlank();
                }
            }

            // determine indentation if not specified
            if (0 === $indentation) {
                if (preg_match('/^ +/', $this->currentLine, $matches)) {
                    $indentation = strlen($matches[0]);
                }
            }

            if ($indentation > 0) {
                $pattern = sprintf('/^ {%d}(.*)$/', $indentation);

                while (
                    $notEOF && (
                        $isCurrentLineBlank ||
                        preg_match($pattern, $this->currentLine, $matches)
                    )
                ) {
                    if ($isCurrentLineBlank) {
                        $text .= substr($this->currentLine, $indentation);
                    } else {
                        $text .= $matches[1];
                    }

                    // newline only if not EOF
                    if ($notEOF = $this->moveToNextLine()) {
                        $text .= "\n";
                        $isCurrentLineBlank = $this->isCurrentLineBlank();
                    }
                }
            } elseif ($notEOF) {
                $text .= "\n";
            }

            if ($notEOF) {
                $this->moveToPreviousLine();
            }

            // folded style
            if ('>' === $style) {
                // folded lines
                // replace all non-leading/non-trailing single newlines with spaces
                preg_match('/(\n*)$/', $text, $matches);
                $text = preg_replace('/(?<!\n|^)\n(?!\n)/', ' ', rtrim($text, "\n"));
                $text .= $matches[1];

                // empty separation lines
                // remove one newline from each group of non-leading/non-trailing newlines
                $text = preg_replace('/[^\n]\n+\K\n(?=[^\n])/', '', $text);
            }

            // deal with trailing newlines
            if ('' === $chomping) {
                $text = preg_replace('/\n+$/', "\n", $text);
            } elseif ('-' === $chomping) {
                $text = preg_replace('/\n+$/', '', $text);
            }

            return $text;
        }

        /**
         * Returns true if the next line is indented.
         *
         * @return bool Returns true if the next line is indented, false otherwise
         */
        private function isNextLineIndented() {
            $currentIndentation = $this->getCurrentLineIndentation();
            $EOF = !$this->moveToNextLine();

            while (!$EOF && $this->isCurrentLineEmpty()) {
                $EOF = !$this->moveToNextLine();
            }

            if ($EOF) {
                return false;
            }

            $ret = false;
            if ($this->getCurrentLineIndentation() > $currentIndentation) {
                $ret = true;
            }

            $this->moveToPreviousLine();

            return $ret;
        }

        /**
         * Returns true if the current line is blank or if it is a comment line.
         *
         * @return bool Returns true if the current line is empty or if it is a comment line, false otherwise
         */
        private function isCurrentLineEmpty() {
            return $this->isCurrentLineBlank() || $this->isCurrentLineComment();
        }

        /**
         * Returns true if the current line is blank.
         *
         * @return bool Returns true if the current line is blank, false otherwise
         */
        private function isCurrentLineBlank() {
            return '' == trim($this->currentLine, ' ');
        }

        /**
         * Returns true if the current line is a comment line.
         *
         * @return bool Returns true if the current line is a comment line, false otherwise
         */
        private function isCurrentLineComment() {
            //checking explicitly the first char of the trim is faster than loops or strpos
            $ltrimmedLine = ltrim($this->currentLine, ' ');

            return $ltrimmedLine[0] === '#';
        }

        /**
         * Cleanups a YAML string to be parsed.
         *
         * @param string $value The input YAML string
         *
         * @return string A cleaned up YAML string
         */
        private function cleanup($value) {
            $value = str_replace(array("\r\n", "\r"), "\n", $value);

            // strip YAML header
            $count = 0;
            $value = preg_replace('#^\%YAML[: ][\d\.]+.*\n#u', '', $value, -1, $count);
            $this->offset += $count;

            // remove leading comments
            $trimmedValue = preg_replace('#^(\#.*?\n)+#s', '', $value, -1, $count);
            if ($count == 1) {
                // items have been removed, update the offset
                $this->offset += substr_count($value, "\n") - substr_count($trimmedValue, "\n");
                $value = $trimmedValue;
            }

            // remove start of the document marker (---)
            $trimmedValue = preg_replace('#^\-\-\-.*?\n#s', '', $value, -1, $count);
            if ($count == 1) {
                // items have been removed, update the offset
                $this->offset += substr_count($value, "\n") - substr_count($trimmedValue, "\n");
                $value = $trimmedValue;

                // remove end of the document marker (...)
                $value = preg_replace('#\.\.\.\s*$#', '', $value);
            }

            return $value;
        }

        /**
         * Returns true if the next line starts unindented collection.
         *
         * @return bool Returns true if the next line starts unindented collection, false otherwise
         */
        private function isNextLineUnIndentedCollection() {
            $currentIndentation = $this->getCurrentLineIndentation();
            $notEOF = $this->moveToNextLine();

            while ($notEOF && $this->isCurrentLineEmpty()) {
                $notEOF = $this->moveToNextLine();
            }

            if (false === $notEOF) {
                return false;
            }

            $ret = false;
            if (
                $this->getCurrentLineIndentation() == $currentIndentation
                &&
                $this->isStringUnIndentedCollectionItem($this->currentLine)
            ) {
                $ret = true;
            }

            $this->moveToPreviousLine();

            return $ret;
        }

        /**
         * Returns true if the string is un-indented collection item.
         *
         * @return bool Returns true if the string is un-indented collection item, false otherwise
         */
        private function isStringUnIndentedCollectionItem() {
            return (0 === strpos($this->currentLine, '- '));
        }

    }

    class Inline {
        const REGEX_QUOTED_STRING = '(?:"([^"\\\\]*(?:\\\\.[^"\\\\]*)*)"|\'([^\']*(?:\'\'[^\']*)*)\')';

        private static $exceptionOnInvalidType = false;
        private static $objectSupport = false;
        private static $objectForMap = false;

        /**
         * Converts a YAML string to a PHP array.
         *
         * @param string $value                  A YAML string
         * @param bool   $exceptionOnInvalidType true if an exception must be thrown on invalid types (a PHP resource or object), false otherwise
         * @param bool   $objectSupport          true if object support is enabled, false otherwise
         * @param bool   $objectForMap           true if maps should return a stdClass instead of array()
         * @param array  $references             Mapping of variable names to values
         *
         * @return array A PHP array representing the YAML string
         *
         * @throws ParseException
         */
        public static function parse($value, $exceptionOnInvalidType = false, $objectSupport = false, $objectForMap = false, $references = array())
        {
            self::$exceptionOnInvalidType = $exceptionOnInvalidType;
            self::$objectSupport = $objectSupport;
            self::$objectForMap = $objectForMap;

            $value = trim($value);

            if ('' === $value) {
                return '';
            }

            if (2 /* MB_OVERLOAD_STRING */ & (int) ini_get('mbstring.func_overload')) {
                $mbEncoding = mb_internal_encoding();
                mb_internal_encoding('ASCII');
            }

            $i = 0;
            switch ($value[0]) {
                case '[':
                    $result = self::parseSequence($value, $i, $references);
                    ++$i;
                    break;
                case '{':
                    $result = self::parseMapping($value, $i, $references);
                    ++$i;
                    break;
                default:
                    $result = self::parseScalar($value, null, array('"', "'"), $i, true, $references);
            }

            // some comments are allowed at the end
            if (preg_replace('/\s+#.*$/A', '', substr($value, $i))) {
                throw new ParseException(sprintf('Unexpected characters near "%s".', substr($value, $i)));
            }

            if (isset($mbEncoding)) {
                mb_internal_encoding($mbEncoding);
            }

            return $result;
        }

        /**
         * Dumps a given PHP variable to a YAML string.
         *
         * @param mixed $value                  The PHP variable to convert
         * @param bool  $exceptionOnInvalidType true if an exception must be thrown on invalid types (a PHP resource or object), false otherwise
         * @param bool  $objectSupport          true if object support is enabled, false otherwise
         *
         * @return string The YAML string representing the PHP array
         *
         * @throws DumpException When trying to dump PHP resource
         */
        public static function dump($value, $exceptionOnInvalidType = false, $objectSupport = false)
        {
            switch (true) {
                case is_resource($value):
                    if ($exceptionOnInvalidType) {
                        throw new DumpException(sprintf('Unable to dump PHP resources in a YAML file ("%s").', get_resource_type($value)));
                    }

                    return 'null';
                case is_object($value):
                    if ($objectSupport) {
                        return '!!php/object:'.serialize($value);
                    }

                    if ($exceptionOnInvalidType) {
                        throw new DumpException('Object support when dumping a YAML file has been disabled.');
                    }

                    return 'null';
                case is_array($value):
                    return self::dumpArray($value, $exceptionOnInvalidType, $objectSupport);
                case null === $value:
                    return 'null';
                case true === $value:
                    return 'true';
                case false === $value:
                    return 'false';
                case ctype_digit($value):
                    return is_string($value) ? "'$value'" : (int) $value;
                case is_numeric($value):
                    $locale = setlocale(LC_NUMERIC, 0);
                    if (false !== $locale) {
                        setlocale(LC_NUMERIC, 'C');
                    }
                    if (is_float($value)) {
                        $repr = (string) $value;
                        if (is_infinite($value)) {
                            $repr = str_ireplace('INF', '.Inf', $repr);
                        } elseif (floor($value) == $value && $repr == $value) {
                            // Preserve float data type since storing a whole number will result in integer value.
                            $repr = '!!float '.$repr;
                        }
                    } else {
                        $repr = is_string($value) ? "'$value'" : (string) $value;
                    }
                    if (false !== $locale) {
                        setlocale(LC_NUMERIC, $locale);
                    }

                    return $repr;
                case '' == $value:
                    return "''";
                case Escaper::requiresDoubleQuoting($value):
                    return Escaper::escapeWithDoubleQuotes($value);
                case Escaper::requiresSingleQuoting($value):
                case preg_match(self::getHexRegex(), $value):
                case preg_match(self::getTimestampRegex(), $value):
                    return Escaper::escapeWithSingleQuotes($value);
                default:
                    return $value;
            }
        }

        /**
         * Dumps a PHP array to a YAML string.
         *
         * @param array $value                  The PHP array to dump
         * @param bool  $exceptionOnInvalidType true if an exception must be thrown on invalid types (a PHP resource or object), false otherwise
         * @param bool  $objectSupport          true if object support is enabled, false otherwise
         *
         * @return string The YAML string representing the PHP array
         */
        private static function dumpArray($value, $exceptionOnInvalidType, $objectSupport)
        {
            // array
            $keys = array_keys($value);
            $keysCount = count($keys);
            if ((1 === $keysCount && '0' == $keys[0])
                || ($keysCount > 1 && array_reduce($keys, function ($v, $w) { return (int) $v + $w; }, 0) === $keysCount * ($keysCount - 1) / 2)
            ) {
                $output = array();
                foreach ($value as $val) {
                    $output[] = self::dump($val, $exceptionOnInvalidType, $objectSupport);
                }

                return sprintf('[%s]', implode(', ', $output));
            }

            // mapping
            $output = array();
            foreach ($value as $key => $val) {
                $output[] = sprintf('%s: %s', self::dump($key, $exceptionOnInvalidType, $objectSupport), self::dump($val, $exceptionOnInvalidType, $objectSupport));
            }

            return sprintf('{ %s }', implode(', ', $output));
        }

        /**
         * Parses a scalar to a YAML string.
         *
         * @param string $scalar
         * @param string $delimiters
         * @param array  $stringDelimiters
         * @param int    &$i
         * @param bool   $evaluate
         * @param array  $references
         *
         * @return string A YAML string
         *
         * @throws ParseException When malformed inline YAML string is parsed
         *
         * @internal
         */
        public static function parseScalar($scalar, $delimiters = null, $stringDelimiters = array('"', "'"), &$i = 0, $evaluate = true, $references = array())
        {
            if (in_array($scalar[$i], $stringDelimiters)) {
                // quoted scalar
                $output = self::parseQuotedScalar($scalar, $i);

                if (null !== $delimiters) {
                    $tmp = ltrim(substr($scalar, $i), ' ');
                    if (!in_array($tmp[0], $delimiters)) {
                        throw new ParseException(sprintf('Unexpected characters (%s).', substr($scalar, $i)));
                    }
                }
            } else {
                // "normal" string
                if (!$delimiters) {
                    $output = substr($scalar, $i);
                    $i += strlen($output);

                    // remove comments
                    if (preg_match('/[ \t]+#/', $output, $match, PREG_OFFSET_CAPTURE)) {
                        $output = substr($output, 0, $match[0][1]);
                    }
                } elseif (preg_match('/^(.+?)('.implode('|', $delimiters).')/', substr($scalar, $i), $match)) {
                    $output = $match[1];
                    $i += strlen($output);
                } else {
                    throw new ParseException(sprintf('Malformed inline YAML string (%s).', $scalar));
                }

                // a non-quoted string cannot start with @ or ` (reserved)
                if ($output && ('@' === $output[0] || '`' === $output[0])) {
                    @trigger_error(sprintf('Not quoting a scalar starting with "%s" is deprecated since Symfony 2.8 and will throw a ParseException in 3.0.', $output[0]), E_USER_DEPRECATED);

                    // to be thrown in 3.0
                    // throw new ParseException(sprintf('The reserved indicator "%s" cannot start a plain scalar; you need to quote the scalar.', $output[0]));
                }

                if ($evaluate) {
                    $output = self::evaluateScalar($output, $references);
                }
            }

            return $output;
        }

        /**
         * Parses a quoted scalar to YAML.
         *
         * @param string $scalar
         * @param int    &$i
         *
         * @return string A YAML string
         *
         * @throws ParseException When malformed inline YAML string is parsed
         */
        private static function parseQuotedScalar($scalar, &$i)
        {
            if (!preg_match('/'.self::REGEX_QUOTED_STRING.'/Au', substr($scalar, $i), $match)) {
                throw new ParseException(sprintf('Malformed inline YAML string (%s).', substr($scalar, $i)));
            }

            $output = substr($match[0], 1, strlen($match[0]) - 2);

            $unescaper = new Unescaper();
            if ('"' == $scalar[$i]) {
                $output = $unescaper->unescapeDoubleQuotedString($output);
            } else {
                $output = $unescaper->unescapeSingleQuotedString($output);
            }

            $i += strlen($match[0]);

            return $output;
        }

        /**
         * Parses a sequence to a YAML string.
         *
         * @param string $sequence
         * @param int    &$i
         * @param array  $references
         *
         * @return string A YAML string
         *
         * @throws ParseException When malformed inline YAML string is parsed
         */
        private static function parseSequence($sequence, &$i = 0, $references = array())
        {
            $output = array();
            $len = strlen($sequence);
            ++$i;

            // [foo, bar, ...]
            while ($i < $len) {
                switch ($sequence[$i]) {
                    case '[':
                        // nested sequence
                        $output[] = self::parseSequence($sequence, $i, $references);
                        break;
                    case '{':
                        // nested mapping
                        $output[] = self::parseMapping($sequence, $i, $references);
                        break;
                    case ']':
                        return $output;
                    case ',':
                    case ' ':
                        break;
                    default:
                        $isQuoted = in_array($sequence[$i], array('"', "'"));
                        $value = self::parseScalar($sequence, array(',', ']'), array('"', "'"), $i, true, $references);

                        // the value can be an array if a reference has been resolved to an array var
                        if (!is_array($value) && !$isQuoted && false !== strpos($value, ': ')) {
                            // embedded mapping?
                            try {
                                $pos = 0;
                                $value = self::parseMapping('{'.$value.'}', $pos, $references);
                            } catch (\InvalidArgumentException $e) {
                                // no, it's not
                            }
                        }

                        $output[] = $value;

                        --$i;
                }

                ++$i;
            }

            throw new ParseException(sprintf('Malformed inline YAML string %s', $sequence));
        }

        /**
         * Parses a mapping to a YAML string.
         *
         * @param string $mapping
         * @param int    &$i
         * @param array  $references
         *
         * @return string A YAML string
         *
         * @throws ParseException When malformed inline YAML string is parsed
         */
        private static function parseMapping($mapping, &$i = 0, $references = array())
        {
            $output = array();
            $len = strlen($mapping);
            ++$i;

            // {foo: bar, bar:foo, ...}
            while ($i < $len) {
                switch ($mapping[$i]) {
                    case ' ':
                    case ',':
                        ++$i;
                        continue 2;
                    case '}':
                        if (self::$objectForMap) {
                            return (object) $output;
                        }

                        return $output;
                }

                // key
                $key = self::parseScalar($mapping, array(':', ' '), array('"', "'"), $i, false);

                // value
                $done = false;

                while ($i < $len) {
                    switch ($mapping[$i]) {
                        case '[':
                            // nested sequence
                            $value = self::parseSequence($mapping, $i, $references);
                            // Spec: Keys MUST be unique; first one wins.
                            // Parser cannot abort this mapping earlier, since lines
                            // are processed sequentially.
                            if (!isset($output[$key])) {
                                $output[$key] = $value;
                            }
                            $done = true;
                            break;
                        case '{':
                            // nested mapping
                            $value = self::parseMapping($mapping, $i, $references);
                            // Spec: Keys MUST be unique; first one wins.
                            // Parser cannot abort this mapping earlier, since lines
                            // are processed sequentially.
                            if (!isset($output[$key])) {
                                $output[$key] = $value;
                            }
                            $done = true;
                            break;
                        case ':':
                        case ' ':
                            break;
                        default:
                            $value = self::parseScalar($mapping, array(',', '}'), array('"', "'"), $i, true, $references);
                            // Spec: Keys MUST be unique; first one wins.
                            // Parser cannot abort this mapping earlier, since lines
                            // are processed sequentially.
                            if (!isset($output[$key])) {
                                $output[$key] = $value;
                            }
                            $done = true;
                            --$i;
                    }

                    ++$i;

                    if ($done) {
                        continue 2;
                    }
                }
            }

            throw new ParseException(sprintf('Malformed inline YAML string %s', $mapping));
        }

        /**
         * Evaluates scalars and replaces magic values.
         *
         * @param string $scalar
         * @param array  $references
         *
         * @return string A YAML string
         *
         * @throws ParseException when object parsing support was disabled and the parser detected a PHP object or when a reference could not be resolved
         */
        private static function evaluateScalar($scalar, $references = array())
        {
            $scalar = trim($scalar);
            $scalarLower = strtolower($scalar);

            if (0 === strpos($scalar, '*')) {
                if (false !== $pos = strpos($scalar, '#')) {
                    $value = substr($scalar, 1, $pos - 2);
                } else {
                    $value = substr($scalar, 1);
                }

                // an unquoted *
                if (false === $value || '' === $value) {
                    throw new ParseException('A reference must contain at least one character.');
                }

                if (!array_key_exists($value, $references)) {
                    throw new ParseException(sprintf('Reference "%s" does not exist.', $value));
                }

                return $references[$value];
            }

            switch (true) {
                case 'null' === $scalarLower:
                case '' === $scalar:
                case '~' === $scalar:
                    return;
                case 'true' === $scalarLower:
                    return true;
                case 'false' === $scalarLower:
                    return false;
                // Optimise for returning strings.
                case $scalar[0] === '+' || $scalar[0] === '-' || $scalar[0] === '.' || $scalar[0] === '!' || is_numeric($scalar[0]):
                    switch (true) {
                        case 0 === strpos($scalar, '!str'):
                            return (string) substr($scalar, 5);
                        case 0 === strpos($scalar, '! '):
                            return (int) self::parseScalar(substr($scalar, 2));
                        case 0 === strpos($scalar, '!!php/object:'):
                            if (self::$objectSupport) {
                                return unserialize(substr($scalar, 13));
                            }

                            if (self::$exceptionOnInvalidType) {
                                throw new ParseException('Object support when parsing a YAML file has been disabled.');
                            }

                            return;
                        case 0 === strpos($scalar, '!!float '):
                            return (float) substr($scalar, 8);
                        case ctype_digit($scalar):
                            $raw = $scalar;
                            $cast = (int) $scalar;

                            return '0' == $scalar[0] ? octdec($scalar) : (((string) $raw == (string) $cast) ? $cast : $raw);
                        case '-' === $scalar[0] && ctype_digit(substr($scalar, 1)):
                            $raw = $scalar;
                            $cast = (int) $scalar;

                            return '0' == $scalar[1] ? octdec($scalar) : (((string) $raw === (string) $cast) ? $cast : $raw);
                        case is_numeric($scalar):
                        case preg_match(self::getHexRegex(), $scalar):
                            return '0x' === $scalar[0].$scalar[1] ? hexdec($scalar) : (float) $scalar;
                        case '.inf' === $scalarLower:
                        case '.nan' === $scalarLower:
                            return -log(0);
                        case '-.inf' === $scalarLower:
                            return log(0);
                        case preg_match('/^(-|\+)?[0-9,]+(\.[0-9]+)?$/', $scalar):
                            return (float) str_replace(',', '', $scalar);
                        case preg_match(self::getTimestampRegex(), $scalar):
                            return strtotime($scalar);
                    }
                default:
                    return (string) $scalar;
            }
        }

        /**
         * Gets a regex that matches a YAML date.
         *
         * @return string The regular expression
         *
         * @see http://www.yaml.org/spec/1.2/spec.html#id2761573
         */
        private static function getTimestampRegex()
        {
            return <<<EOF
            ~^
            (?P<year>[0-9][0-9][0-9][0-9])
            -(?P<month>[0-9][0-9]?)
            -(?P<day>[0-9][0-9]?)
            (?:(?:[Tt]|[ \t]+)
            (?P<hour>[0-9][0-9]?)
            :(?P<minute>[0-9][0-9])
            :(?P<second>[0-9][0-9])
            (?:\.(?P<fraction>[0-9]*))?
            (?:[ \t]*(?P<tz>Z|(?P<tz_sign>[-+])(?P<tz_hour>[0-9][0-9]?)
            (?::(?P<tz_minute>[0-9][0-9]))?))?)?
            $~x
EOF;
        }

        /**
         * Gets a regex that matches a YAML number in hexadecimal notation.
         *
         * @return string
         */
        private static function getHexRegex()
        {
            return '~^0x[0-9a-f]++$~i';
        }
    }

    class Escaper {
        // Characters that would cause a dumped string to require double quoting.
        const REGEX_CHARACTER_TO_ESCAPE = "[\\x00-\\x1f]|\xc2\x85|\xc2\xa0|\xe2\x80\xa8|\xe2\x80\xa9";

        // Mapping arrays for escaping a double quoted string. The backslash is
        // first to ensure proper escaping because str_replace operates iteratively
        // on the input arrays. This ordering of the characters avoids the use of strtr,
        // which performs more slowly.
        private static $escapees = array('\\', '\\\\', '\\"', '"',
            "\x00",  "\x01",  "\x02",  "\x03",  "\x04",  "\x05",  "\x06",  "\x07",
            "\x08",  "\x09",  "\x0a",  "\x0b",  "\x0c",  "\x0d",  "\x0e",  "\x0f",
            "\x10",  "\x11",  "\x12",  "\x13",  "\x14",  "\x15",  "\x16",  "\x17",
            "\x18",  "\x19",  "\x1a",  "\x1b",  "\x1c",  "\x1d",  "\x1e",  "\x1f",
            "\xc2\x85", "\xc2\xa0", "\xe2\x80\xa8", "\xe2\x80\xa9",);
        private static $escaped = array('\\\\', '\\"', '\\\\', '\\"',
            '\\0',   '\\x01', '\\x02', '\\x03', '\\x04', '\\x05', '\\x06', '\\a',
            '\\b',   '\\t',   '\\n',   '\\v',   '\\f',   '\\r',   '\\x0e', '\\x0f',
            '\\x10', '\\x11', '\\x12', '\\x13', '\\x14', '\\x15', '\\x16', '\\x17',
            '\\x18', '\\x19', '\\x1a', '\\e',   '\\x1c', '\\x1d', '\\x1e', '\\x1f',
            '\\N', '\\_', '\\L', '\\P',);

        /**
         * Determines if a PHP value would require double quoting in YAML.
         *
         * @param string $value A PHP value
         *
         * @return bool True if the value would require double quotes.
         */
        public static function requiresDoubleQuoting($value)
        {
            return preg_match('/'.self::REGEX_CHARACTER_TO_ESCAPE.'/u', $value);
        }

        /**
         * Escapes and surrounds a PHP value with double quotes.
         *
         * @param string $value A PHP value
         *
         * @return string The quoted, escaped string
         */
        public static function escapeWithDoubleQuotes($value)
        {
            return sprintf('"%s"', str_replace(self::$escapees, self::$escaped, $value));
        }

        /**
         * Determines if a PHP value would require single quoting in YAML.
         *
         * @param string $value A PHP value
         *
         * @return bool True if the value would require single quotes.
         */
        public static function requiresSingleQuoting($value)
        {
            // Determines if a PHP value is entirely composed of a value that would
            // require single quoting in YAML.
            if (in_array(strtolower($value), array('null', '~', 'true', 'false', 'y', 'n', 'yes', 'no', 'on', 'off'))) {
                return true;
            }

            // Determines if the PHP value contains any single characters that would
            // cause it to require single quoting in YAML.
            return preg_match('/[ \s \' " \: \{ \} \[ \] , & \* \# \?] | \A[ \- ? | < > = ! % @ ` ]/x', $value);
        }

        /**
         * Escapes and surrounds a PHP value with single quotes.
         *
         * @param string $value A PHP value
         *
         * @return string The quoted, escaped string
         */
        public static function escapeWithSingleQuotes($value)
        {
            return sprintf("'%s'", str_replace('\'', '\'\'', $value));
        }
    }

    class Unescaper {
        /**
         * Regex fragment that matches an escaped character in a double quoted string.
         */
        const REGEX_ESCAPED_CHARACTER = "\\\\(x[0-9a-fA-F]{2}|u[0-9a-fA-F]{4}|U[0-9a-fA-F]{8}|.)";

        /**
         * Unescapes a single quoted string.
         *
         * @param string $value A single quoted string.
         *
         * @return string The unescaped string.
         */
        public function unescapeSingleQuotedString($value)
        {
            return str_replace('\'\'', '\'', $value);
        }

        /**
         * Unescapes a double quoted string.
         *
         * @param string $value A double quoted string.
         *
         * @return string The unescaped string.
         */
        public function unescapeDoubleQuotedString($value)
        {
            $callback = function ($match) {
                return $this->unescapeCharacter($match[0]);
            };

            // evaluate the string
            return preg_replace_callback('/'.self::REGEX_ESCAPED_CHARACTER.'/u', $callback, $value);
        }

        /**
         * Unescapes a character that was found in a double-quoted string.
         *
         * @param string $value An escaped character
         *
         * @return string The unescaped character
         */
        private function unescapeCharacter($value)
        {
            switch ($value[1]) {
                case '0':
                    return "\x0";
                case 'a':
                    return "\x7";
                case 'b':
                    return "\x8";
                case 't':
                    return "\t";
                case "\t":
                    return "\t";
                case 'n':
                    return "\n";
                case 'v':
                    return "\xB";
                case 'f':
                    return "\xC";
                case 'r':
                    return "\r";
                case 'e':
                    return "\x1B";
                case ' ':
                    return ' ';
                case '"':
                    return '"';
                case '/':
                    return '/';
                case '\\':
                    return '\\';
                case 'N':
                    // U+0085 NEXT LINE
                    return "\xC2\x85";
                case '_':
                    // U+00A0 NO-BREAK SPACE
                    return "\xC2\xA0";
                case 'L':
                    // U+2028 LINE SEPARATOR
                    return "\xE2\x80\xA8";
                case 'P':
                    // U+2029 PARAGRAPH SEPARATOR
                    return "\xE2\x80\xA9";
                case 'x':
                    return self::utf8chr(hexdec(substr($value, 2, 2)));
                case 'u':
                    return self::utf8chr(hexdec(substr($value, 2, 4)));
                case 'U':
                    return self::utf8chr(hexdec(substr($value, 2, 8)));
                default:
                    throw new ParseException(sprintf('Found unknown escape character "%s".', $value));
            }
        }

        /**
         * Get the UTF-8 character for the given code point.
         *
         * @param int $c The unicode code point
         *
         * @return string The corresponding UTF-8 character
         */
        private static function utf8chr($c)
        {
            if (0x80 > $c %= 0x200000) {
                return chr($c);
            }
            if (0x800 > $c) {
                return chr(0xC0 | $c >> 6).chr(0x80 | $c & 0x3F);
            }
            if (0x10000 > $c) {
                return chr(0xE0 | $c >> 12).chr(0x80 | $c >> 6 & 0x3F).chr(0x80 | $c & 0x3F);
            }

            return chr(0xF0 | $c >> 18).chr(0x80 | $c >> 12 & 0x3F).chr(0x80 | $c >> 6 & 0x3F).chr(0x80 | $c & 0x3F);
        }
    }
