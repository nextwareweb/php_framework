<?php
    /* Define Workspace */
    $workspace = 'development';

    /* Core Requires */
    require('defines.php');
    require('functions.php');
    require('autoload.php');

    /* Initialize Core Object */
    $coreName = APP_NAME;
    $coreClassName = APP_NAME.'_class';
    $$coreName = new $coreClassName();
    core()->init($workspace);

//pre(core(), true);